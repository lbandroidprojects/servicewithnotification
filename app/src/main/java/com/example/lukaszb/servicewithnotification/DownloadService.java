package com.example.lukaszb.servicewithnotification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DownloadService extends Service {
    private String path;
    private final int notificatiobId=1;
    private Notification.Builder builder;
    private NotificationManager nm;
    Intent receiveIntent;
    public DownloadService() {
        builder=new Notification.Builder(this);
        nm=(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        receiveIntent=new Intent(this,ReceiveActivity.class);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        path=intent.getStringExtra("path");
        DownloadThread dt=new DownloadThread();
        dt.start();
        try {
            dt.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    class DownloadThread extends Thread{


        @Override
        public void run() {
            HttpURLConnection connection=null;
            super.run();
            try {
                URL url = new URL(path);
                connection=(HttpURLConnection) url.openConnection();
            }
            catch(MalformedURLException e){
               e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            initDownloadNotification();
            try(BufferedInputStream bis=new BufferedInputStream(connection.getInputStream());
                FileOutputStream fos=new FileOutputStream(getLocalDownloadFolder()+"/"+extractFileName())){
                double fileSize=connection.getContentLength();
                int buffSize=1024;
                byte[] buffer=new byte[buffSize];
                double currentSize=0;
                int count;
                while((count=bis.read(buffer,0,buffSize))>-1){
                    fos.write(buffer,0,count);
                    currentSize+=count;
                    int downloadPercent=(int)(100*currentSize/fileSize);
                    updateDownloadNotification(downloadPercent);
                }

            }
            catch(IOException e){
                e.printStackTrace();
            }
            finishDownloadNotification();
        }
    }

    private String getLocalDownloadFolder(){
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
    }

    private String extractFileName()
    {
        return path.substring(path.lastIndexOf("/")+1);
    }

    private void initDownloadNotification(){
        builder.setSmallIcon(android.R.drawable.stat_sys_download).setContentTitle("File download").setContentText("in progress..");
        PendingIntent pendingIntent=PendingIntent.getActivity(this,1,receiveIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        nm.notify(notificatiobId,builder.build());
    }

    private void finishDownloadNotification(){
        builder.setSmallIcon(android.R.drawable.stat_sys_download_done).setContentText("Finished");
        nm.notify(notificatiobId,builder.build());
    }

    private void updateDownloadNotification(int progress){
        builder.setProgress(100,progress,false);
        nm.notify(notificatiobId,builder.build());
    }
}
