package com.example.lukaszb.servicewithnotification;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final String path="https://cran.r-project.org/doc/manuals/r-release/R-intro.pdf";
    private Button b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b=(Button)findViewById(R.id.button);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(isNetworkActive()) {
            Intent i = new Intent(this, DownloadService.class);
            i.putExtra("path", path);
            startService(i);
        }
        else
            Toast.makeText(this,"Brak połączenia sieciowego",Toast.LENGTH_SHORT).show();
    }

    private boolean isNetworkActive(){
        boolean isActive=false;
        ConnectivityManager cm=(ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo ni=cm.getActiveNetworkInfo();
          if(ni!=null)
              isActive= ni.isAvailable() && ni.isConnected();

        return isActive;
    }
}
